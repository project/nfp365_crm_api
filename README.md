# NfP365 CRM API

## Setup

After installing the module, you will need to provide credentials 
for OpenApi and WebApi.

Go to `/admin/config/services/nfp365-crm-api` and provide credentials.

## Examples

```php
// Request via OpenApi client.
/* @var \Drupal\nfp365_crm_api\Http\OpenApiClient $openApi */
$openApi = \Drupal::service('nfp365_crm_api.manager')->getOpenApiClient();
$response = $openApi->paymentProcessors()->all();
foreach ($response->PaymentProcessors as $paymentProcessor) {
  //
}

// Request via WebApi client.
/* @var \Drupal\nfp365_crm_api\Http\WebApiClient $webApi */
$webApi = \Drupal::service('nfp365_crm_api.manager')->getWebApiClient();
$response = $webApi->campaigns()->all();
$campaigns = $response->getData();
foreach ($campaigns as $campaign) {
  //
}

// Other useful methods are available for $response. 
$response->getStatusCode();   // 200;
$response->getReasonPhrase(); // 'OK';
// etc...

```

## How to add a new Resource
An example for OpenAPI:
1. Create a class and place it in `/src/Resources/OpenAPI/` folder.
2. Describe a new method in `/src/Http/OpenApiClient.php`
