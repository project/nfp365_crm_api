<?php

namespace Drupal\nfp365_crm_api;

use Drupal\nfp365_crm_api\Http\WebApiClient;
use Drupal\nfp365_crm_api\Http\OpenApiClient;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;

/**
 * NfP365 CRM API Manager.
 */
class Manager implements ManagerInterface {

  /**
   * The config for NfP365 CRM API.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * Logger service.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * Constructs a new NfP365 CRM API service instance.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factory
   *   The logger factory.
   */
  public function __construct(ConfigFactoryInterface $config_factory, LoggerChannelFactoryInterface $logger_factory) {
    $this->config = $config_factory->get('nfp365_crm_api.config');
    $this->logger = $logger_factory->get('nfp365_crm_api');
  }

  /**
   * {@inheritdoc}
   */
  public function getOpenApiClient() {
    $api_url = $this->config->get('open_api.api_url');
    $api_key = $this->config->get('open_api.api_key');
    $enabled = $this->config->get('open_api.enabled');
    $timeout = $this->config->get('open_api.timeout');
    $debugConfig = $this->config->get('debug');

    if (!$enabled) {
      $this->logger->info('NfP365 CRM OpenAPI disabled.');
      return NULL;
    }

    if (empty($api_url) || empty($api_key)) {
      $this->logger->error('Missing NfP365 CRM OpenAPI configuration.');
      return NULL;
    }

    $config = [
      'api_url' => $api_url,
      'api_key' => $api_key,
      'debug' => $debugConfig,
    ];

    $client_options = [];
    if (!empty($timeout)) {
      $client_options['timeout'] = $timeout;
    }

    return new OpenApiClient($config, $client_options);
  }

  /**
   * {@inheritdoc}
   */
  public function getWebApiClient() {
    $api_url = $this->config->get('web_api.api_url');
    $username = $this->config->get('web_api.username');
    $password = $this->config->get('web_api.password');
    $enabled = $this->config->get('web_api.enabled');
    $timeout = $this->config->get('web_api.timeout');
    $debugConfig = $this->config->get('debug');

    if (!$enabled) {
      $this->logger->info('NfP365 CRM WebAPI disabled.');
      return NULL;
    }

    if (empty($api_url) || empty($username) || empty($password)) {
      $this->logger->error('Missing NfP365 CRM WebAPI configuration.');
      return NULL;
    }

    $config = [
      'api_url' => $api_url,
      'username' => $username,
      'password' => $password,
      'debug' => $debugConfig,
    ];

    $client_options = [];
    if (!empty($timeout)) {
      $client_options['timeout'] = $timeout;
    }

    return new WebApiClient($config, $client_options);
  }

}
