<?php

namespace Drupal\nfp365_crm_api\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Path\PathValidatorInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class ConfigForm.
 *
 * @package Drupal\nfp365_crm_api\Form
 */
class ConfigForm extends ConfigFormBase {

  /**
   * The path validator.
   *
   * @var \Drupal\Core\Path\PathValidatorInterface
   */
  protected $pathValidator;

  /**
   * Constructs a \Drupal\system\ConfigFormBase object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\Core\Path\PathValidatorInterface $path_validator
   *   The path validator class.
   */
  public function __construct(ConfigFactoryInterface $config_factory, PathValidatorInterface $path_validator) {
    parent::__construct($config_factory);
    $this->pathValidator = $path_validator;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('path.validator')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'nfp365_crm_api.config',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'nfp365_crm_api_config_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('nfp365_crm_api.config');

    // Settings for Open API.
    $form['open_api'] = [
      '#type' => 'fieldset',
      '#collapsible' => FALSE,
      '#title' => $this->t('Open API'),
      '#tree' => TRUE,
    ];
    $form['open_api']['enabled'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enabled'),
      '#default_value' => $config->get('open_api.enabled'),
    ];
    $form['open_api']['api_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API Base URL'),
      '#description' => $this->t('URI of the Open API server without trailing slash.'),
      '#maxlength' => 255,
      '#size' => 255,
      '#default_value' => $config->get('open_api.api_url'),
    ];
    $form['open_api']['api_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API Key'),
      '#description' => $this->t('API Key used for authentication to connect to Open API.'),
      '#maxlength' => 255,
      '#size' => 255,
      '#default_value' => $config->get('open_api.api_key'),
    ];
    $form['open_api']['timeout'] = [
      '#type' => 'number',
      '#title' => $this->t('Timeout'),
      '#description' => $this->t('Timeout (in seconds) for API requests. Put "0" for no timeout.'),
      '#maxlength' => 3,
      '#default_value' => $config->get('open_api.timeout') ?? 60,
    ];

    // Settings for Web API.
    $form['web_api'] = [
      '#type' => 'fieldset',
      '#collapsible' => FALSE,
      '#title' => $this->t('Web API'),
      '#tree' => TRUE,
    ];
    $form['web_api']['enabled'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enabled'),
      '#default_value' => $config->get('web_api.enabled'),
    ];
    $form['web_api']['api_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API Base URL'),
      '#description' => $this->t('URI of the Web API server without trailing slash.'),
      '#maxlength' => 255,
      '#size' => 255,
      '#default_value' => $config->get('web_api.api_url'),
    ];
    $form['web_api']['username'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Username'),
      '#maxlength' => 255,
      '#size' => 255,
      '#default_value' => $config->get('web_api.username'),
    ];
    $form['web_api']['password'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Password'),
      '#maxlength' => 255,
      '#size' => 255,
      '#default_value' => $config->get('web_api.password'),
    ];
    $form['web_api']['timeout'] = [
      '#type' => 'number',
      '#title' => $this->t('Timeout'),
      '#description' => $this->t('Timeout (in seconds) for API requests. Put "0" for no timeout.'),
      '#maxlength' => 3,
      '#default_value' => $config->get('web_api.timeout') ?? 60,
    ];

    $form['debug'] = [
      '#type' => 'fieldset',
      '#collapsible' => FALSE,
      '#title' => $this->t('Debug settings'),
      '#tree' => TRUE,
    ];
    $form['debug']['verbose'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Verbose logging'),
      '#default_value' => $config->get('debug.verbose'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $open_api_url = $form_state->getValue('open_api')['api_url'];
    $web_api_url = $form_state->getValue('web_api')['api_url'];

    // Validate OpenApi base url.
    if ($open_api_url !== '' && !$this->pathValidator->isValid($open_api_url)) {
      $form_state->setErrorByName('open_api[api_url', $this->t('Open API Url should be a valid url.'));
    }
    // Validate WebApi base url.
    if ($web_api_url !== '' && !$this->pathValidator->isValid($web_api_url)) {
      $form_state->setErrorByName('web_api[api_url', $this->t('Web API Url should be a valid url.'));
    }

    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('nfp365_crm_api.config')
      ->set('open_api', $form_state->getValue('open_api'))
      ->set('web_api', $form_state->getValue('web_api'))
      ->set('debug', $form_state->getValue('debug'))
      ->save();
  }

}
