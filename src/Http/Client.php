<?php

namespace Drupal\nfp365_crm_api\Http;

use GuzzleHttp\Client as GuzzleClient;

/**
 * Basic Api Client class.
 */
abstract class Client {

  /**
   * Default request timeout.
   */
  const REQUEST_TIMEOUT = 60;

  /**
   * Client instance.
   *
   * @var \GuzzleHttp\Client
   */
  public $client;

  /**
   * Options that will be passed to request method.
   *
   * @var array
   */
  protected $clientOptions = [];

  /**
   * Debug config.
   *
   * @var array
   */
  protected $debugConfig = [];

  /**
   * Api URL.
   *
   * @var string
   */
  protected $api_url;

  /**
   * The logger.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * Class constructor.
   *
   * @param array $config
   *   Client configurations.
   * @param array $clientOptions
   *   Options to be passed to Guzzle upon each request.
   */
  public function __construct(array $config = [], array $clientOptions = []) {
    $this->clientOptions = $clientOptions;
    // @todo: move to DI.
    $this->logger = \Drupal::logger('nfp365_crm_api');

    if (empty($config['api_url'])) {
      $message = t('You must provide NfP365 CRM API Base url.');
      $this->logger->error($message);
      throw new \Exception($message);
    }
    $this->api_url = $config['api_url'];

    $this->debugConfig = $config['debug'];

    // Initialize the Client.
    $this->client = new GuzzleClient(['timeout' => self::REQUEST_TIMEOUT]);
  }

  /**
   * Send the request.
   *
   * @param string $method
   *   The HTTP request verb.
   * @param string $endpoint
   *   The NfP365 CRM API endpoint (doesn't include base url).
   * @param array $options
   *   An array of options to send with the request.
   *
   * @return \Drupal\nfp365_crm_api\Http\Response
   *   Response object.
   */
  public function request($method, $endpoint, array $options = []) {
    $options = array_merge($this->clientOptions, $options);

    try {
      $url = $this->api_url . $endpoint;

      // Adds necessary headers.
      $options['headers']['accept'] = 'application/json';
      $options['headers']['Content-Type'] = 'application/json';

      if (!empty($this->debugConfig['verbose'])) {
        $this->logger
          ->debug(t('NfP365 API Request: %method %url. Data: <pre>%data</pre>', [
            '%method' => $method,
            '%url' => $url,
            '%data' => !empty($options['body']) ? json_encode(json_decode($options['body']), JSON_PRETTY_PRINT) : '',
          ]));
      }

      $response = $this->client->request($method, $url, $options);

      // Wrap to the custom Response class for useful functions.
      $wrapped_response = new Response($response);

      if (!empty($this->debugConfig['verbose'])) {
        $data_for_log = $wrapped_response->getData();
        $additional_message = 'Data :';
        if (is_array($data_for_log) && count($data_for_log) > 20) {
          $data_for_log = array_slice($data_for_log, 0, 20);
          $additional_message = 'First 20 elements from data:';
        }
        $this->logger
          ->debug(t('NfP365 API Response: %url. Status: %status. %additional_message <pre>%data</pre>', [
            '%url' => $url,
            '%status' => $wrapped_response->getStatusCode(),
            '%additional_message' => $additional_message,
            '%data' => json_encode($data_for_log, JSON_PRETTY_PRINT),
          ]));
      }

      return $wrapped_response;
    }
    catch (\Exception $e) {
      $message = $this->sanitizeErrorMessage($e->getMessage());

      $this->logger->debug('There is a problem during CURL request. Error message: @error', [
        '@error' => $message . ' Trace: ' . $e->getTraceAsString(),
      ]);

      throw new \Exception($message, $e->getCode(), $e);
    }
  }

  /**
   * Utility function to sanitize messages.
   *
   * @param string $message
   *   The message.
   *
   * @return string
   *   The sanitized message.
   */
  protected function sanitizeErrorMessage(string $message): string {
    // Pattern for data to be removed from the message.
    $pattern = '/code=([a-zA-Z0-9%]+)/';
    // Perform the replacement.
    return preg_replace($pattern, 'code=******', $message);
  }

}
