<?php

namespace Drupal\nfp365_crm_api\Http;

/**
 * Class OpenApiClient.
 *
 * @method \Drupal\nfp365_crm_api\Resources\OpenApi\PaymentProcessors paymentProcessors()
 * @method \Drupal\nfp365_crm_api\Resources\OpenApi\Donors donors()
 * @method \Drupal\nfp365_crm_api\Resources\OpenApi\Income income()
 * @method \Drupal\nfp365_crm_api\Resources\OpenApi\Tax tax()
 * @method \Drupal\nfp365_crm_api\Resources\OpenApi\Orders orders()
 */
class OpenApiClient extends Client {

  /**
   * {@inheritdoc}
   */
  public function __construct($config = [], $clientOptions = []) {
    if (empty($config['api_key'])) {
      $message = t('You must provide NfP365 CRM API key.');
      \Drupal::logger('nfp365_crm_api')->error($message);
      throw new \Exception($message);
    }
    // Attaches secret code to each request.
    $clientOptions['query']['code'] = $config['api_key'];

    parent::__construct($config, $clientOptions);
  }

  /**
   * Return an instance of a Resource based on the method called.
   *
   * @param string $name
   *   Name of the method.
   * @param array $arguments
   *   Arguments that will be passed further.
   *
   * @return \Drupal\nfp365_crm_api\Resources\Resource
   *   Resource object.
   */
  public function __call($name, ?array $arguments = NULL) {
    $resource = 'Drupal\\nfp365_crm_api\\Resources\\OpenApi\\' . ucfirst($name);

    return new $resource($this);
  }

}
