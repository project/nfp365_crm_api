<?php

namespace Drupal\nfp365_crm_api\Http;

/**
 * Class WebApiClient.
 *
 * @method \Drupal\nfp365_crm_api\Resources\WebApi\Campaigns campaigns()
 * @method \Drupal\nfp365_crm_api\Resources\WebApi\PreferencesDeclarations preferencesDeclarations()
 * @method \Drupal\nfp365_crm_api\Resources\WebApi\Consents consents()
 * @method \Drupal\nfp365_crm_api\Resources\WebApi\PaymentFrequencies paymentFrequencies()
 */
class WebApiClient extends Client {

  /**
   * {@inheritdoc}
   */
  public function __construct($config = [], $clientOptions = []) {
    if (empty($config['username']) || empty($config['password'])) {
      $message = t('You must provide username and password for NfP365 CRM API authentication.');
      \Drupal::logger('nfp365_crm_api')->error($message);
      throw new \Exception($message);
    }

    // Attaches authentication header to each request.
    $token = base64_encode($config['username'] . ':' . $config['password']);
    $clientOptions['headers']['Authorization'] = 'Basic ' . $token;
    parent::__construct($config, $clientOptions);
  }

  /**
   * Return an instance of a Resource based on the method called.
   *
   * @param string $name
   *   Name of the method.
   * @param array $arguments
   *   Arguments that will be passed further.
   *
   * @return \Drupal\nfp365_crm_api\Resources\Resource
   *   Resource object.
   */
  public function __call($name, ?array $arguments = NULL) {
    $resource = 'Drupal\\nfp365_crm_api\\Resources\\WebApi\\' . ucfirst($name);

    return new $resource($this);
  }

}
