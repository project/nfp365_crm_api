<?php

namespace Drupal\nfp365_crm_api\Http;

use Psr\Http\Message\StreamInterface;
use Psr\Http\Message\ResponseInterface;

/**
 * Class Response.
 */
class Response implements ResponseInterface {
  /**
   * Response instance.
   *
   * @var \Psr\Http\Message\ResponseInterface
   */
  protected $response;

  /**
   * Response data.
   *
   * @var mixed
   */
  public $data;

  /**
   * Class constructor.
   *
   * @param \Psr\Http\Message\ResponseInterface $response
   *   Response instance.
   */
  public function __construct(ResponseInterface $response) {
    $this->response = $response;
    $this->data = $this->getDataFromResponse($response);
  }

  /**
   * Get the api data from the response as usual.
   *
   * @param string $name
   *   Name of the property.
   *
   * @return mixed
   *   Response data.
   */
  public function __get($name) {
    return $this->data->$name;
  }

  /**
   * Returns raw response data.
   *
   * @param \Psr\Http\Message\ResponseInterface $response
   *   Response instance.
   *
   * @return mixed
   *   Raw Response data.
   */
  private function getDataFromResponse(ResponseInterface $response) {
    $contents = $response->getBody()->getContents();

    return $contents ? \GuzzleHttp\json_decode($contents) : NULL;
  }

  /**
   * Get the underlying data.
   *
   * @return mixed
   *   Raw Response data.
   */
  public function getData() {
    return $this->data;
  }

  /**
   * Return an array of the data.
   *
   * @return array
   *   Response data in array format.
   */
  public function toArray() {
    return \GuzzleHttp\json_decode(\GuzzleHttp\json_encode($this->data), TRUE);
  }

  /**
   * {@inheritdoc}
   */
  public function getProtocolVersion() {
    return $this->response->getProtocolVersion();
  }

  /**
   * {@inheritdoc}
   */
  public function withProtocolVersion($version) {
    return $this->response->withProtocolVersion($version);
  }

  /**
   * {@inheritdoc}
   */
  public function getHeaders() {
    return $this->response->getHeaders();
  }

  /**
   * {@inheritdoc}
   */
  public function hasHeader($name) {
    return $this->response->hasHeader($name);
  }

  /**
   * {@inheritdoc}
   */
  public function getHeader($name) {
    return $this->response->getHeader($name);
  }

  /**
   * {@inheritdoc}
   */
  public function getHeaderLine($name) {
    return $this->response->getHeaderLine($name);
  }

  /**
   * {@inheritdoc}
   */
  public function withHeader($name, $value) {
    return $this->response->withHeader($name, $value);
  }

  /**
   * {@inheritdoc}
   */
  public function withAddedHeader($name, $value) {
    return $this->response->withAddedHeader($name, $value);
  }

  /**
   * {@inheritdoc}
   */
  public function withoutHeader($name) {
    return $this->response->withoutHeader($name);
  }

  /**
   * {@inheritdoc}
   */
  public function getBody() {
    return $this->response->getBody();
  }

  /**
   * {@inheritdoc}
   */
  public function withBody(StreamInterface $body) {
    return $this->response->withBody($body);
  }

  /**
   * {@inheritdoc}
   */
  public function getStatusCode() {
    return $this->response->getStatusCode();
  }

  /**
   * {@inheritdoc}
   */
  public function withStatus($code, $reasonPhrase = '') {
    return $this->response->withStatus($code, $reasonPhrase);
  }

  /**
   * {@inheritdoc}
   */
  public function getReasonPhrase() {
    return $this->response->getReasonPhrase();
  }

}
