<?php

namespace Drupal\nfp365_crm_api;

/**
 * Interface ManagerInterface.
 *
 * @package Drupal\nfp365_crm_api
 */
interface ManagerInterface {

  /**
   * Returns new OpenApi Client instance.
   *
   * @return \Drupal\nfp365_crm_api\Http\OpenApiClient
   *   OpenApi Client instance.
   */
  public function getOpenApiClient();

  /**
   * Returns new WebApi Client instance.
   *
   * @return \Drupal\nfp365_crm_api\Http\WebApiClient
   *   WebApi Client instance.
   */
  public function getWebApiClient();

}
