<?php

namespace Drupal\nfp365_crm_api\Resources\OpenApi;

use Drupal\nfp365_crm_api\Resources\Resource;

/**
 * Orders class.
 */
class Orders extends Resource {

  /**
   * Create order.
   *
   * @return \Drupal\nfp365_crm_api\Http\Response
   *   Response object containing Order Id and Order number.
   */
  public function create($data) {
    $endpoint = '/api/orders';

    return $this->client->request('post', $endpoint, ['body' => \GuzzleHttp\json_encode($data)]);
  }

  /**
   * Fulfill order.
   *
   * @return \Drupal\nfp365_crm_api\Http\Response
   *   Response.
   */
  public function fulfill($order_id) {
    $endpoint = "/api/orders/$order_id/fulfill";

    return $this->client->request('post', $endpoint);
  }

  /**
   * Get price lists.
   *
   * @return \Drupal\nfp365_crm_api\Http\Response
   *   Response.
   */
  public function pricelists() {
    $endpoint = "/api/orders/pricelists";

    return $this->client->request('get', $endpoint);
  }

}
