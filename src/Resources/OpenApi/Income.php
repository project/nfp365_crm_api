<?php

namespace Drupal\nfp365_crm_api\Resources\OpenApi;

use Drupal\nfp365_crm_api\Resources\Resource;

/**
 * Income class.
 */
class Income extends Resource {

  /**
   * Create Donation.
   *
   * @return \Drupal\nfp365_crm_api\Http\Response
   *   Response object containing Donation Id.
   */
  public function createDonation($data) {
    $endpoint = '/api/donation';

    return $this->client->request('post', $endpoint, ['body' => \GuzzleHttp\json_encode($data)]);
  }

  /**
   * Create Pledge.
   *
   * @return \Drupal\nfp365_crm_api\Http\Response
   *   Response object containing Pledge Id.
   */
  public function createPledge($data) {
    $endpoint = '/api/createpledge';

    return $this->client->request('post', $endpoint, ['body' => \GuzzleHttp\json_encode($data)]);
  }

}
