<?php

namespace Drupal\nfp365_crm_api\Resources\OpenApi;

use Drupal\nfp365_crm_api\Resources\Resource;

/**
 * PaymentProcessors class.
 */
class PaymentProcessors extends Resource {

  /**
   * Get all Payment Processors.
   *
   * @return \Drupal\nfp365_crm_api\Http\Response
   *   Response object containing Payment processors data.
   */
  public function all() {
    $endpoint = '/api/paymentprocessors';

    return $this->client->request('get', $endpoint);
  }

}
