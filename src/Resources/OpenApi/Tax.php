<?php

namespace Drupal\nfp365_crm_api\Resources\OpenApi;

use Drupal\nfp365_crm_api\Resources\Resource;

/**
 * Tax class.
 */
class Tax extends Resource {

  /**
   * Get all tax authorities.
   *
   * @return \Drupal\nfp365_crm_api\Http\Response
   *   Response object containing authorities data.
   */
  public function authorities() {
    $endpoint = '/api/tax/authorities';

    return $this->client->request('get', $endpoint);
  }

  /**
   * Create tax declaration.
   *
   * @param array $data
   *   Tax declaration payload.
   *
   * @return \Drupal\nfp365_crm_api\Http\Response
   *   Response object containing tax data.
   *
   * @throws \Exception
   */
  public function create(array $data) {
    $endpoint = '/api/tax/reliefdeclaration';

    return $this->client->request('post', $endpoint, ['body' => \GuzzleHttp\json_encode($data)]);
  }

}
