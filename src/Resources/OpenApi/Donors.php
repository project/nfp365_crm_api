<?php

namespace Drupal\nfp365_crm_api\Resources\OpenApi;

use Drupal\nfp365_crm_api\Resources\Resource;

/**
 * Donors class.
 */
class Donors extends Resource {

  /**
   * Retrieves details of the CRM Donor record associated with given email.
   *
   * @param string $email
   *   An email for lookup.
   *
   * @return \Drupal\nfp365_crm_api\Http\Response
   *   Response object containing full Donor data.
   */
  public function getDonorByEmail($email) {
    $endpoint = "/api/donorbyemail/$email";

    return $this->client->request('get', $endpoint);
  }

  /**
   * Retrieves details of the CRM Donor record associated with the supplied id.
   *
   * @param string $donor_id
   *   Unique identifier for the contact.
   *
   * @return \Drupal\nfp365_crm_api\Http\Response
   *   Response object containing full Donor data.
   */
  public function getDonorById($donor_id) {
    $endpoint = "/api/donorbyid/$donor_id";

    return $this->client->request('get', $endpoint);
  }

  /**
   * Retrieves data of individual donor identified by the supplied parameters.
   *
   * CRM is searched for values in the order they are shown in the table below.
   * i.e. If EmailAddress is supplied but SupporterId is not, EmailAddress is
   * used to search CRM first. Once a match has been made with CRM other
   * supplied filters are applied to the records retrieved from CRM.
   *
   * @return \Drupal\nfp365_crm_api\Http\Response
   *   Response object containing Donor data or
   *   throw exception if user doesn't exist.
   */
  public function getDonorByParams($data) {
    $endpoint = '/api/donor';

    return $this->client->request('post', $endpoint, ['body' => \GuzzleHttp\json_encode($data)]);
  }

  /**
   * Create or update donor.
   *
   * Leave contactId empty to create a new donor, otherwise donor with ContactId
   * will be updated with given data.
   *
   * @return \Drupal\nfp365_crm_api\Http\Response
   *   Response object containing Donor Id.
   */
  public function createOrUpdateDonor($data) {
    $endpoint = '/api/createorupdatedonor';

    return $this->client->request('post', $endpoint, ['body' => \GuzzleHttp\json_encode($data)]);
  }

  /**
   * Retrieves available titles for CRM Donor.
   *
   * @return \Drupal\nfp365_crm_api\Http\Response
   *   Response object containing all available titles.
   */
  public function getTitles() {
    $endpoint = "/api/titles";

    return $this->client->request('get', $endpoint);
  }

}
