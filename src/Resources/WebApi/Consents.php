<?php

namespace Drupal\nfp365_crm_api\Resources\WebApi;

use Drupal\nfp365_crm_api\Resources\Resource;

/**
 * Consents class.
 */
class Consents extends Resource {

  /**
   * Creates multiple consents at once.
   *
   * @param array $data
   *   Payload for /api/v1/consents/batch endpoint.
   *
   * @return \Drupal\nfp365_crm_api\Http\Response
   *   Response from the API.
   *
   * @throws \Exception
   */
  public function createMultiple(array $data) {
    $endpoint = '/api/v1/consents/batch';

    return $this->client->request('post', $endpoint, ['body' => \GuzzleHttp\json_encode($data)]);
  }

  /**
   * Get list of exercises.
   *
   * @return \Drupal\nfp365_crm_api\Http\Response
   *   Response from the API.
   *
   * @throws \Exception
   */
  public function exercises() {
    $endpoint = '/api/v1/consents/exercises';

    return $this->client->request('get', $endpoint);
  }

  /**
   * Get list of media.
   *
   * @return \Drupal\nfp365_crm_api\Http\Response
   *   Response from the API.
   *
   * @throws \Exception
   */
  public function media() {
    $endpoint = '/api/v1/consents/media';

    return $this->client->request('get', $endpoint);
  }


}
