<?php

namespace Drupal\nfp365_crm_api\Resources\WebApi;

use Drupal\nfp365_crm_api\Resources\Resource;

/**
 * PreferencesDeclarations class.
 */
class PreferencesDeclarations extends Resource {

  /**
   * Get all PreferencesDeclarations sources.
   *
   * @return \Drupal\nfp365_crm_api\Http\Response
   *   Response object containing PreferencesDeclarations sources data.
   *
   * @throws \Exception
   */
  public function sources() {
    $endpoint = '/api/v1/preferencesdeclarations/sources';

    return $this->client->request('get', $endpoint);
  }

  /**
   * Creates new preferences declaration.
   *
   * @param array $data
   *   Preferences Declaration payload.
   *
   * @return \Drupal\nfp365_crm_api\Http\Response
   *   Response object from the API.
   *
   * @throws \Exception
   */
  public function create(array $data) {
    $endpoint = '/api/v1/preferencesdeclarations';

    return $this->client->request('post', $endpoint, ['body' => \GuzzleHttp\json_encode($data)]);
  }

}
