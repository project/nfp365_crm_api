<?php

namespace Drupal\nfp365_crm_api\Resources\WebApi;

use Drupal\nfp365_crm_api\Resources\Resource;

/**
 * Campaigns class.
 */
class Campaigns extends Resource {

  /**
   * Get all campaigns.
   *
   * @param array $params
   *   Optional params with customProperties to fetch.
   *
   * @return \Drupal\nfp365_crm_api\Http\Response
   *   Response object containing Campaigns data.
   * @throws \Exception
   */
  public function all($params = []) {
    $endpoint = '/api/v1/campaigns';

    if (!empty($params)) {
      return $this->client->request('post', $endpoint, [
        'body' => \GuzzleHttp\json_encode($params),
        'timeout' => 150,
      ]);
    }

    return $this->client->request('get', $endpoint);
  }

}
