<?php

namespace Drupal\nfp365_crm_api\Resources\WebApi;

use Drupal\nfp365_crm_api\Resources\Resource;

/**
 * Consents class.
 */
class PaymentFrequencies extends Resource {

  /**
   * Get all payment frequencies.
   *
   * @return \Drupal\nfp365_crm_api\Http\Response
   *   Response from the API.
   *
   * @throws \Exception
   */
  public function all() {
    $endpoint = '/api/v1/paymentfrequencies';

    return $this->client->request('get', $endpoint);
  }

}
