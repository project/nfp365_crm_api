<?php

namespace Drupal\nfp365_crm_api\Resources;

use Drupal\nfp365_crm_api\Http\Client;

/**
 * Class Resource.
 */
abstract class Resource {
  /**
   * Client instance.
   *
   * @var \Drupal\nfp365_crm_api\Http\Client
   */
  protected $client;

  /**
   * Class constructor.
   *
   * @param \Drupal\nfp365_crm_api\Http\Client $client
   *   Client instance.
   */
  public function __construct(Client $client) {
    $this->client = $client;
  }

}
